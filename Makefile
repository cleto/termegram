all:
	@echo 'Nothing to do for all'

clean:
	$(RM) $(shell find . -name '*~') $(shell find . -name '*.pyc')
	$(RM) -r __pycache__
