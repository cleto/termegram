import sys
import urwid

import threading

from pytg.sender import Sender
from pytg.receiver import Receiver
from pytg.utils import coroutine


def exit_on_esc(key):
    if key == 'esc':
        raise urwid.ExitMainLoop()


class InputMessageBox(urwid.Edit):
    def __init__(self, tg_sender, user, remote_user, text_box):
        super(InputMessageBox, self).__init__()
        self.text_box = text_box
        self.user = user
        self.remote_user = remote_user
        self.tg_sender = sender

    def keypress(self, size, key):
        if key == 'shift page up':
            print(self.text_box.get_text())
        if key == 'shift page down':
            pass

        if key == 'enter':
            msg = self.edit_text.strip()
            if not msg:
                return
            self.tg_sender.send_msg(self.remote_user, msg)
            self.text_box.body.append(
                urwid.Text('{}: {}'.format(self.user, msg))
            )
            self.set_edit_text('')
            return

        return super(InputMessageBox, self).keypress(size, key)


class MainWindow(urwid.ListBox):
    def __init__(self, sender, user, remote_user):
        self.sender = sender
        self.user = user
        self.remote_user = remote_user
        messages = list(
            filter(
                lambda x: x['event'] == 'message',
                sender.history(self.remote_user, 40)
            )
        )

        message_text_widgets = [
            urwid.Text('{}: {}'.format(
                x['from']['username'], x.get('text'))
            ) for x in messages
        ]
        self.message_listbox = urwid.ListBox(
            urwid.SimpleListWalker(message_text_widgets)
        )
        self.message_listbox.set_focus(len(messages) - 1)
        edit_box = InputMessageBox(
            self.sender, self.user, self.remote_user, self.message_listbox
        )

        super(MainWindow, self).__init__(
            urwid.SimpleListWalker([
                urwid.BoxAdapter(self.message_listbox, 35),
                urwid.Divider('-'),
                edit_box
            ])
        )

    @coroutine
    def new_message(self, loop):
        while True:
            msg = (yield)
            if msg.get('event') != 'message':
                continue

            sender = msg.get('sender', {}).get('username')
            if self.user == sender:
                continue

            if self.remote_user[1:] != sender:
                continue

            self.message_listbox.body.append(
                urwid.Text('{}: {}'.format(self.remote_user[1:], msg['text']))
            )
            self.message_listbox.set_focus(len(self.message_listbox.body) - 1)
            loop.draw_screen()


def start_receiver(receiver, ui, loop):
    receiver.message(ui.new_message(loop))

sender = Sender(host="localhost", port=4458)
receiver = Receiver(host="localhost", port=4458)
receiver.start()


main_window = MainWindow(
    sender, 'clet0', sys.argv[1]
)
main_window.set_focus(2)

loop = urwid.MainLoop(
    urwid.LineBox(main_window, 'Termegram'),
    unhandled_input=exit_on_esc
)

t = threading.Thread(
    target=start_receiver, args=(receiver, main_window, loop), daemon=True
)
t.start()

loop.run()
